/*package com.cjfn.dao;

import java.sql.Connection;
import java.sql.DriverManager;


public class DAO {
    private Connection cn;// variable privada de conexion
    
    public Connection getCn()// variable publica que regresa una conexion
    {
        return cn;// retornar conexion
    }
    
    public void setCn(Connection cn)// variable vacia con parametro de conexion
    {
        this.cn = cn;// referencia a conexion
    }
    
    public void Conectar() throws Exception// variable vacia de conexion con excepcion
    {
        //Connection cn=null;
        try{// trato de conectar
        Class.forName("com.mysql.jdbc.Driver");// incluyo driver de conexion
        //cn = DriverManager.getConnection("dbc:mysql://localhost:3306/facturas",
                    //"root","@CJflorian01");  
        Connection cn = DriverManager.getConnection("jdbc:oracle:thin:@10.27.172.12:1521/XE",
                    "main_ingface","ingAdmin13Face");
        System.out.println("ingresado exitosamente");
        }
        catch(Exception e){
           
            System.out.print("Error en la conexion"+e);
            
        }
        
        
    }
    
    public  void Cerrar() throws Exception
    {
        try{
            if(cn != null)
            {
                if(cn.isClosed()==false){
                    cn.close(); 
                }
            }
            
        }
        catch(Exception e){
             System.out.print("Error en la conexion"+e);
        }
    }
}
*/

package com.cjfn.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class DAO {
    private Connection cn;
    
    public Connection getCn()
    {
        return cn;
    }
    
    public void setCn(Connection cn)
    {
        this.cn = cn;
    }
    
    public void Conectar() throws Exception
    {
        try{
        Class.forName("com.mysql.jdbc.Driver");
        Connection cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/facturas?user=root&password=");    
        }
        catch(Exception e){
            throw e;    
        }
        
    }
    
    public  void Cerrar() throws Exception
    {
        try{
            if(cn != null)
            {
                if(cn.isClosed()==false){
                    cn.close(); 
                }
            }
            
        }
        catch(Exception e){
            throw e;
        }
    }
}


/*
//CONEXION ORACLE
public class DAO {

    public static Connection Conectar(){//no se instancia solo se llama por clase
        Connection cn=null;
        try
        {
            Class.forName("oracle.jdbc.OracleDriver");
            cn = DriverManager.getConnection("jdbc:oracle:thin:@10.27.172.12:1521/XE",
                    "main_ingface","ingAdmin13Face");  
        }
        catch(Exception e)
        {
            System.out.print("Error en la conexion"+e);
        }
        return cn;
    }
    public static void main(String[] args) {
     DAO.getConection();
    }
    
}
*/
