/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cjfn.model;

import java.sql.Date;

/**
 *
 * @author troy
 */
public class Facturas {
    

    private Date fecha;
    private double total;
    private Empresa empresa;

    
    // constructor para listar
    public Facturas(Date fecha, double total, Empresa empresa) {
        this.fecha = fecha;
        this.total = total;
        this.empresa = empresa;
    }
    
    
    
    
    
    
    // metodos getter y setter

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    
    
    
    
}
