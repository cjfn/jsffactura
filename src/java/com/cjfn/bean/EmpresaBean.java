
package com.cjfn.bean;

import com.cjfn.model.Empresa;
import com.cjfn.dao.EmpresaDAO;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@RequestScoped

public class EmpresaBean {
    private Empresa empresa = new Empresa();

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public void registrar() throws Exception{
        EmpresaDAO dao;
        
        try{
            dao = new EmpresaDAO();
            dao.registrar(empresa);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    
}
